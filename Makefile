CFLAGS?=-O2 -g
LDFLAGS?=
CC?=gcc
INSTALL?=install

RODISC_PKGCONFIG:=glib-2.0 gobject-2.0 gio-unix-2.0 libsoup-2.4 avahi-client avahi-gobject
RODISC_CFLAGS:=-Wall $(shell pkg-config --cflags $(RODISC_PKGCONFIG))
RODISC_LIBS:=$(shell pkg-config --libs $(RODISC_PKGCONFIG))

all: rodiscd

rodiscd: rodisc.o server.o mdns.o file.o monitor.o
	$(CC) $(LDFLAGS) -o $@ $+ $(LIBS) $(RODISC_LIBS)

%.o: %.c
	$(CC) $(RODISC_CFLAGS) $(CFLAGS) -o $@ -c $<

clean:
	rm -f *.o rodiscd

install: rodiscd
	$(INSTALL) -s rodiscd $(DESTDIR)/usr/sbin/rodiscd

uninstall:
	rm -f $(DESTDIR)/usr/sbin/rodiscd

install-gentoo: install
	$(INSTALL) rodiscd.init.gentoo $(DESTDIR)/etc/init.d/rodiscd

uninstall-gentoo: uninstall
	rm -f $(DESTDIR)/etc/init.d/rodiscd

.PHONY: all clean install uninstall install-gentoo uninstall-gentoo
