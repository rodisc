#include <stdlib.h>

#include "rodisc.h"

static GMainLoop *main_loop;

static GHashTable *discs;

static gchar **files = NULL;

static GOptionEntry entries[] =
{
    { "file", 'f', 0, G_OPTION_ARG_FILENAME_ARRAY, &files, "Image files to export as remote discs", "FILE"},
    { NULL }
};

RODisc *rodisc_new()
{
	return g_slice_new0(RODisc);
}

void rodisc_destroy(RODisc *disc)
{
	g_free(disc->id);
	g_free(disc->uri);
	g_object_unref(disc->file);
	g_free(disc->file_path);
	g_free(disc->label);
	g_slice_free(RODisc, disc);
}

RODisc *rodisc_lookup(const gchar *id)
{
	return g_hash_table_lookup(discs, id);
}

void rodisc_export(RODisc *disc)
{
	g_debug("Exporting %s to %s (volume '%s' type '%s' size %lu)",
	        disc->file_path, disc->uri, disc->label, disc->type, disc->size);
	g_hash_table_insert(discs, disc->id, disc);

	server_register(disc);

	mdns_publish(disc);
}

void rodisc_remove(RODisc *disc)
{
	g_debug("Unexporting %s", disc->uri);

	mdns_unpublish(disc);

	server_unregister(disc);

	g_hash_table_remove(discs, disc->id);
}

void rodisc_refresh(RODisc *disc)
{
	g_debug("Reexporting %s to %s (volume '%s' type '%s' size %lu)",
	        disc->file_path, disc->uri, disc->label, disc->type, disc->size);
	mdns_publish(disc);
}

static void refresh_all_discs_func(gpointer key, gpointer value, gpointer user_data)
{
	RODisc *disc = (RODisc*) value;
	rodisc_refresh(disc);
}

void rodisc_refresh_all()
{
	g_hash_table_foreach(discs, refresh_all_discs_func, NULL);
}

static void files_add()
{
	gchar **f;
	if (!files) return;
	for (f = files; *f; f++) {
		file_add_disc(*f);
	}
}

int main(int argc, char * argv[])
{
	GError *error = NULL;
	GOptionContext *context = g_option_context_new("- remote optical disc service");

	main_loop = g_main_loop_new(NULL, FALSE);

	discs = g_hash_table_new_full(g_str_hash, g_str_equal,
	                              NULL, (GDestroyNotify) rodisc_destroy);

	g_option_context_add_main_entries(context, entries, NULL);
	if (!g_option_context_parse(context, &argc, &argv, &error)) {
		g_printerr("Option parsing failed: %s\n", error->message);
		return EXIT_FAILURE;
	}

	if (!server_start()) {
		return EXIT_FAILURE;
	}

	if (!mdns_start()) {
		return EXIT_FAILURE;
	}

	if (!monitor_start()) {
		return EXIT_FAILURE;
	}

	files_add();

	g_main_loop_run(main_loop);

	monitor_stop();

	mdns_stop();

	server_stop();

	g_hash_table_destroy(discs);
	g_main_loop_unref(main_loop);
	g_option_context_free(context);

	return EXIT_SUCCESS;
}
