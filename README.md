rodisc
======

This is a very small and hacky daemon (done in ~3 hours) that will export all
optical discs found in your Linux box (as detected by glib) using the
_Apple DVD or CD Sharing_ protocol.

Dependencies
------------

* GLib (recent, with GIO)
* Avahi (avahi-gobject)
* Libsoup

Usage
-----

For automatic mode just run the daemon.

    ./rodiscd

If you want to export image files in addition to your system's CD or DVD discs,
add each image file as -f <path.img>:

    ./rodiscd -f myimage.iso -f myimage2.iso

Security
--------

Potentially a security nightmare -- open web server to everyone.
Best run as a user with permission only to read the CD/DVD block files.
