#ifndef _RODISC_H_
#define _RODISC_H_

#include <stdint.h>
#include <stdbool.h>
#include <gio/gio.h>

#define RODISC_MDNS_SERVICE "_odisk._tcp"

#define RODISC_IMAGE_MIME "application/octet-stream"

#define RODISC_TYPE_GENERIC "public.optical-storage-media"
#define RODISC_TYPE_CD "public.cd-media"
#define RODISC_TYPE_DVD "public.dvd-media"
#define RODISC_TYPE_BD "public.optical-storage-media"

typedef struct {
	char *id;
	char *uri;
	char *file_path;
	GFile *file;
	char *label;
	const char *type;
	uint64_t size;
} RODisc;

RODisc *rodisc_new();
void rodisc_destroy(RODisc *disc);

/** Lookup a given disc id on the exported discs */
RODisc *rodisc_lookup(const gchar *id);
/** Export a given disc object */
void rodisc_export(RODisc *disc);
/** Unexport and destroy a disc */
void rodisc_remove(RODisc *disc);
/** When a disc has changed */
void rodisc_refresh(RODisc *disc);
/** When all exported discs may have changed */
void rodisc_refresh_all();

// HTTP server part
bool server_start();
void server_stop();
void server_register(RODisc *disc);
void server_unregister(RODisc *disc);
unsigned int server_get_port();

// MDNS server part (Bonjour)
bool mdns_start();
void mdns_stop();
void mdns_publish(RODisc *disc);
void mdns_unpublish(RODisc *disc);

/** Export a individual .iso file as a disk. */
void file_add_disc(const char *path);

// Disc change monitor part
bool monitor_start();
void monitor_stop();

#endif /* _RODISC_H_ */
