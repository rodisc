#include "rodisc.h"

void file_add_disc(const char *path)
{
	static int num = 0;
	GFile *file = g_file_new_for_path(path);
	GFileInfo *info = g_file_query_info(file,
										G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME ","
										G_FILE_ATTRIBUTE_STANDARD_SIZE,
	                                    G_FILE_QUERY_INFO_NONE, NULL, NULL);
	g_return_if_fail(info);
	const int my_num = ++num;
	RODisc *disc = rodisc_new();
	disc->id = g_strdup_printf("file%d", my_num);
	disc->uri = g_strdup_printf("/file%d", my_num);
	disc->file_path = g_file_get_path(file);
	disc->file = file;
	disc->label = g_strdup(g_file_info_get_display_name(info));
	disc->type = RODISC_TYPE_GENERIC;
	disc->size = g_file_info_get_size(info);
	rodisc_export(disc);
	g_object_unref(info);
}
